# matching-paren-overlay.el
*display the beginning of your form at the top of the window*

qThis package is based heavily on [this blog post by Clemens
Radermacher](https://with-emacs.com/posts/ui-hacks/show-matching-lines-when-parentheses-go-off-screen/).
I've done very little else with the code, except making it into a minor-mode and
adding a few customization opttions.

## Installation

As of this writing, `matching-paren-overlay` isn't on any ELPA yet.  However,
you can install and use this package by copying `matching-paren-overlay.el` to
your `load-path` and `require`-ing it, or you can use a git-aware package
manager.  For example, I use
[straight.el](https://github.com/radian-software/straight.el) like so:

``` elisp
(straight-use-package '(matching-paren-overlay
                        :repo "https://codeberg.org/acdw/matching-paren-overlay.el"))
```

## Usage

You should just have to call `(matching-paren-overlay-mode)` in your buffers of
choice, or if you want to turn it on in every buffer,
`(matching-paren-overlay-global-mode)`.

## Customization

- `matching-paren-overlay-face` defines the face of the overlay.  It defaults to
  inheriting from `highlight`.
- `matching-paren-overlay-position-function` is the function to call to get a
  window position for the overlay.  The default is `window-start`, but
  `window-end` might also be desirable.

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Contributing

Feel free to [open an
issue](https://codeberg.org/acdw/matching-paren-overlay.el/issues/new) or [start
a pull
request](https://codeberg.org/acdw/matching-paren-overlay.el/compare/main...main)!
I *think* this package should work on Emacs newer than 25.1, but I haven't done
extensive testing.

## Thanks

Massive thanks to Clemens Radermacher for the code here; honestly I'm not even
sure I can take the credit for this package.  It's a great idea and has improved
my Emacs configuration considerably.
