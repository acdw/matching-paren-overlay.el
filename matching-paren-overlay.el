;;; matching-paren-overlay.el --- Show matching parens in an overlay  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Case Duckworth

;; Author: Case Duckworth <case@acdw.net>
;; Package-Version: 0.1.0
;; Package-Requires: ((emacs "25.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Taken from this blog post by Clemens Radermacher:
;; https://with-emacs.com/posts/ui-hacks/show-matching-lines-when-parentheses-go-off-screen/

;;; Code:

(require 'cl-lib)
(require 'paren)

(defgroup matching-paren-overlay nil
  "Display matching parentheses in an overlay."
  :group 'paren-matching
  :prefix "matching-paren-overlay-")

;;;###autoload
(defface matching-paren-overlay-face nil
  "The face for the matching-paren overlay.")

;;;###autoload
(set-face-attribute 'matching-paren-overlay-face nil
                    :inherit 'highlight)

(defcustom matching-paren-overlay-position-function #'window-start
  "A function returning the point at which to display the overlay.")

(defvar-local matching-paren-overlay--ov nil
  "The matching paren overlay.")

(defvar matching-paren-overlay--blink-matching-paren nil
  "The original value of `blink-matching-paren'.")

(defvar matching-paren-overlay--post-self-insert-hook nil
  "The original value of `post-self-insert-hook'.")

(defun matching-paren-overlay--display-line-overlay (pos str)
  "Display line at POS as STR with FACE.
FACE defaults to inheriting from `default' and `highlight'."
  (let ((ol (save-excursion
              (goto-char pos)
              (make-overlay (line-beginning-position)
                            (line-end-position)))))
    (overlay-put ol 'display str)
    (overlay-put ol 'face 'matching-paren-overlay-face)
    ol))

(defun matching-paren-overlay-show (&rest _)
  "Display the matching line for an off-screen paren."
  (when (overlayp matching-paren-overlay--ov)
    (delete-overlay matching-paren-overlay--ov))
  (when (and (overlay-buffer show-paren--overlay)
             (not (or cursor-in-echo-area
                      executing-kbd-macro
                      noninteractive
                      (minibufferp)
                      this-command))
             (and (not (bobp))
                  (memq (char-syntax (char-before)) '(?\) ?\$)))
             (= 1 (logand 1 (- (point)
                               (save-excursion
                                 (forward-char -1)
                                 (skip-syntax-backward "/\\")
                                 (point))))))
    (cl-letf (((symbol-function #'minibuffer-message)
               (lambda (msg &rest args)
                 (let ((msg (apply #'format-message msg args)))
                   (setq matching-paren-overlay--ov
                         (matching-paren-overlay--display-line-overlay
                          (save-excursion
                            (funcall matching-paren-overlay-position-function))
                          msg))))))
      (blink-matching-open))))

;;;###autoload
(define-minor-mode matching-paren-overlay-mode
  "Show the off-screen paren match message as an overlay."
  :lighter "^("
  (cond (matching-paren-overlay-mode
         (setq matching-paren-overlay--post-self-insert-hook post-self-insert-hook
               matching-paren-overlay--blink-matching-paren blink-matching-paren
               blink-matching-paren 'show)
         (remove-hook 'post-self-insert-hook #'blink-paren-post-self-insert-function)
         (advice-add 'show-paren-function :after #'matching-paren-overlay-show)
         (show-paren-mode +1))
        (t
         (setq post-self-insert-hook matching-paren-overlay--post-self-insert-hook
               blink-matching-paren matching-paren-overlay--blink-matching-paren)
         (advice-remove 'show-paren-function #'matching-paren-overlay-show)
         (show-paren-mode (if (default-value 'show-paren-mode) +1 -1)))))

;;;###autoload
(define-globalized-minor-mode matching-paren-overlay-global-mode
  matching-paren-overlay-mode
  matching-paren-overlay-mode)

(provide 'matching-paren-overlay)
;;; matching-paren-overlay.el ends here
